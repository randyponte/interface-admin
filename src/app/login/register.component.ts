import { Component, OnInit } from '@angular/core';
import { TokenService } from '../service/token.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isLogged = false;


  constructor(

    private tokenService: TokenService,


  ) { }

  ngOnInit(): void {

    if (this.tokenService.getToken()) {
      this.isLogged = true;
    }

  }

}
