import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Jwt } from '../models/jwt';
import { Login } from '../models/login';
import { AuthService } from '../service/auth.service';
import { TokenService } from '../service/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    isLogged = false;
    isLoginFail = false;
    login: Login;
    userName: string;
    password: string;
    errorMsj: string;

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()){
      this.isLogged = true;
      this.isLoginFail = false;
    }
  }

  onLogin(): void{
    this.login = new Login(this.userName , this.password);
    
    this.authService.login(this.login).subscribe (
      data => {
        this.isLogged = true;
        this.isLoginFail = false;
        console.log(data.jwt);
        
        
        //convertir el jwt en JSON y setear aquí
        this.tokenService.setToken(data.jwt);
        this.tokenService.setSub(data.sub);
        this.tokenService.setName(data.name);
        this.tokenService.setIat(data.iat);
        this.router.navigate(['/']);
      },
        error => {
          this.isLogged = false;
          this.isLoginFail = true;
          this.errorMsj =  error.mensaje;
          console.log(this.errorMsj);
        }
    );
  }
}
