import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Register } from '../models/register';
import { Login } from '../models/login';
import { Jwt } from '../models/jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL = 'http://nirvana-admin-engine-git-engine-dev.apps.ocptest.gp.inet/';

  constructor(private httpClient: HttpClient ) { }

  public register(register: Register): Observable<any>{
    return this.httpClient.post<any>(this.authURL + 'register' , register);
  }

  public login(login: Login): Observable<any>{
    return this.httpClient.post<Jwt>(this.authURL + 'authenticate' , login);
  }
}
