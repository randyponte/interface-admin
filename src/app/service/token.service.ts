import { Injectable } from '@angular/core';

const TOKEN_KEY = 'AuthToken';
const SUB_KEY = 'AuthSub';
const NAME_KEY = 'AuthName';
const IAT_KEY = 'AuthIat';


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

//token
public setToken(token: string): void {
  window.sessionStorage.removeItem(TOKEN_KEY);
  window.sessionStorage.setItem(TOKEN_KEY , token);
}

public getToken(): string {
  return sessionStorage.getItem(TOKEN_KEY);
}


//SUB
public setSub(sub: string): void {
  window.sessionStorage.removeItem(SUB_KEY);
  window.sessionStorage.setItem(SUB_KEY, sub);
}

public getSub(): string {
  return sessionStorage.getItem(SUB_KEY);
}


//EXP
public setName(name: string): void {
  window.sessionStorage.removeItem(NAME_KEY);
  window.sessionStorage.setItem(NAME_KEY, name);
}

public getName(): string {
  return sessionStorage.getItem(NAME_KEY);
}



//IAT
public setIat(Iat: string): void {
  window.sessionStorage.removeItem(IAT_KEY);
  window.sessionStorage.setItem(IAT_KEY, Iat);
}

public getIat(): string {
  return sessionStorage.getItem(IAT_KEY);
}


public logOut(): void {
   window.sessionStorage.clear();
 }


}
