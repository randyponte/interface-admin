import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [


  { path : 'login' , component : LoginComponent},
  { path : 'register' , component : RegisterComponent},
  { path : 'dashboard' , component : DashboardComponent},
  { path : '' , component : IndexComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
