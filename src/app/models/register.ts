export class Register {

    userName: string;
    password: string;
    email: string;
    phone: string;

    constructor(userName: string, password: string, email: string, phone: string){
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.phone = phone;

    }

}
